package com.unitbv.main;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.unitbv.model.*;
import com.unitbv.database.Database;
import com.unitbv.database.DatabaseConnection;
import com.unitbv.util.ClientOperation;
import com.unitbv.util.DealerOperation;
import com.unitbv.util.EmployeeOperation;
import com.unitbv.util.MedicineOperation;
import com.unitbv.util.PrescriptionOperation;


public class MainClass {
	public static void main(String[] args) throws NumberFormatException, IOException {
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/drugstore", "root",
				"1q2w3e");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		DealerOperation dealerOperation = new DealerOperation(databaseConnection);
		ClientOperation clientOperation = new ClientOperation(databaseConnection);
		EmployeeOperation employeeOperation = new EmployeeOperation(databaseConnection);
		MedicineOperation medicineOperation = new MedicineOperation(databaseConnection);
		PrescriptionOperation prescriptionOperation = new PrescriptionOperation(databaseConnection);
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


		while (true) {
			System.out.println("Optiuni:");
			System.out.println("1. CRUD Dealer.");
			System.out.println("2. CRUD Client.");
			System.out.println("3. CRUD Employee.");
			System.out.println("4. CRUD Medicine");
			System.out.println("5. CRUD Prescription.");
			System.out.println("6. JOINS.");
			int option = Integer.parseInt(reader.readLine());
			switch (option) {

			case 0:
				System.exit(0);
				break;
			case 1:
				System.out.println("Alegeti o optiune: ");
				System.out.println("1.Create.");
				System.out.println("2.Update.");
				System.out.println("3.Delete.");
				System.out.println("4.Print.");
				int optDealer = Integer.parseInt(reader.readLine());
				switch (optDealer) {

				case 0:
					break;
				case 1:
					System.out.println("Id: ");
					int idDealer = Integer.parseInt(reader.readLine());
					System.out.println("Numele Dealer-ului: ");
					String dealerName = reader.readLine();
					System.out.println("Adresa ");
					String address = reader.readLine();
					System.out.println("Numar de telefon: ");
					String phoneNumber = reader.readLine();
					Dealer dealer = new Dealer(idDealer, dealerName, address, phoneNumber);
					dealerOperation.addDealer(dealer);
					break;
				case 2:
					System.out.println("Id: ");
					int idDealerU = Integer.parseInt(reader.readLine());
					System.out.println("Numele Dealer-ului: ");
					String dealerNameU = reader.readLine();
					System.out.println("Adresa ");
					String addressU = reader.readLine();
					System.out.println("Numar de telefon: ");
					String phoneNumberU = reader.readLine();
					dealerOperation.updateDealer(idDealerU, dealerNameU, addressU, phoneNumberU);
					break;

				case 3:
					System.out.println("Id: ");
					int idDealerD = Integer.parseInt(reader.readLine());
					dealerOperation.deleteDealer(idDealerD);
					break;
				case 4:
					List<Dealer> dealers = new ArrayList<Dealer>();
					dealers = dealerOperation.getAllDealers();
					dealerOperation.printListOfDealers(dealers);
					break;
				}
				break;
			case 2:
				System.out.println("Alegeti o optiune: ");
				System.out.println("1.Create.");
				System.out.println("2.Update.");
				System.out.println("3.Delete.");
				System.out.println("4.Print.");
				int optClient = Integer.parseInt(reader.readLine());
				switch (optClient) {

				case 0:
					break;
				case 1:
					System.out.println("Id: ");
					int idClient = Integer.parseInt(reader.readLine());
					System.out.println("Numele Clientului: ");
					String lastName = reader.readLine();
					System.out.println("Prenumele Clientului: ");
					String firstName = reader.readLine();
					System.out.println("Numar de telefon: ");
					String phoneNumber = reader.readLine();
					System.out.println("Id-ul prescriptiei: ");
					int idPrescription = Integer.parseInt(reader.readLine());
					Client client = new Client(idClient, firstName, lastName, phoneNumber, idPrescription);
					clientOperation.addClient(client);
					break;
				case 2:
					System.out.println("Id: ");
					int idClientU = Integer.parseInt(reader.readLine());
					System.out.println("Numele Clientului: ");
					String lastNameU = reader.readLine();
					System.out.println("Prenumele Clientului: ");
					String firstNameU = reader.readLine();
					System.out.println("Numar de telefon: ");
					String phoneNumberU = reader.readLine();
					System.out.println("Id-ul prescriptiei: ");
					int idPrescriptionU = Integer.parseInt(reader.readLine());
					clientOperation.updateClient(idClientU, firstNameU, lastNameU, phoneNumberU, idPrescriptionU);
					break;

				case 3:
					System.out.println("Id: ");
					int idClientD = Integer.parseInt(reader.readLine());
					clientOperation.deleteClient(idClientD);
					break;
				case 4:
					List<Client> clients = new ArrayList<Client>();
					clients = clientOperation.getAllClients();
					clientOperation.printListOfClients(clients);
					break;
				}
				break;
				
			case 3:
				System.out.println("Alegeti o optiune: ");
				System.out.println("1.Create.");
				System.out.println("2.Update.");
				System.out.println("3.Delete.");
				System.out.println("4.Print.");
				int optEmployee = Integer.parseInt(reader.readLine());
				switch (optEmployee) {

				case 0:
					break;
				case 1:
					System.out.println("Id: ");
					int idEmployee = Integer.parseInt(reader.readLine());
					System.out.println("Numele Angajatului: ");
					String lastName = reader.readLine();
					System.out.println("Prenumele Angajatului: ");
					String firstName = reader.readLine();
					System.out.println("Numar de telefon: ");
					String phoneNumber = reader.readLine();
					Employee employee = new Employee(idEmployee, firstName, lastName, phoneNumber);
					employeeOperation.addEmployee(employee);
					break;
				case 2:
					System.out.println("Id: ");
					int idEmployeeU = Integer.parseInt(reader.readLine());
					System.out.println("Numele Angajatului: ");
					String lastNameU = reader.readLine();
					System.out.println("Prenumele Angajatului: ");
					String firstNameU = reader.readLine();
					System.out.println("Numar de telefon: ");
					String phoneNumberU = reader.readLine();
					employeeOperation.updateEmployee(idEmployeeU, firstNameU, lastNameU, phoneNumberU);
					break;

				case 3:
					System.out.println("Id: ");
					int idEmployeeD = Integer.parseInt(reader.readLine());
					employeeOperation.deleteEmployee(idEmployeeD);
					break;
				case 4:
					List<Employee> employees = new ArrayList<Employee>();
					employees = employeeOperation.getAllEmployees();
					employeeOperation.printListOfEmployees(employees);
					break;
				}
				break;
			case 4:
				System.out.println("Alegeti o optiune: ");
				System.out.println("1.Create.");
				System.out.println("2.Update.");
				System.out.println("3.Delete.");
				System.out.println("4.Print.");
				int optMedicine = Integer.parseInt(reader.readLine());
				switch (optMedicine) {

				case 0:
					break;
				case 1:
					System.out.println("Id: ");
					int idMedicine = Integer.parseInt(reader.readLine());
					System.out.println("Numele Medicamentului: ");
					String medicineName = reader.readLine();
					System.out.println("Pretul: ");
					int price = Integer.parseInt(reader.readLine());
					System.out.println("Id Dealer: ");
					int idDealer = Integer.parseInt(reader.readLine());
					Medicine medicine = new Medicine(idMedicine, medicineName, price, idDealer);
					medicineOperation.addMedicine(medicine);
					break;
				case 2:
					System.out.println("Id: ");
					int idMedicineU = Integer.parseInt(reader.readLine());
					System.out.println("Numele Medicamentului: ");
					String medicineNameU = reader.readLine();
					System.out.println("Pretul: ");
					int priceU = Integer.parseInt(reader.readLine());
					System.out.println("Id Dealer: ");
					int idDealerU = Integer.parseInt(reader.readLine());
					medicineOperation.updateMedicine(idMedicineU, medicineNameU, priceU, idDealerU);
					break;

				case 3:
					System.out.println("Id: ");
					int idMedicineD = Integer.parseInt(reader.readLine());
					medicineOperation.deleteMedicine(idMedicineD);
					break;
				case 4:
					List<Medicine> medicines = new ArrayList<Medicine>();
					medicines = medicineOperation.getAllMedicines();
					medicineOperation.printListOfMedicines(medicines);
					break;
				}
				break;
			case 5:
				System.out.println("Alegeti o optiune: ");
				System.out.println("1.Create.");
				System.out.println("2.Update.");
				System.out.println("3.Delete.");
				System.out.println("4.Print.");
				int optPrescription = Integer.parseInt(reader.readLine());
				switch (optPrescription) {

				case 0:
					break;
				case 1:
					System.out.println("Id: ");
					int idPrescription = Integer.parseInt(reader.readLine());
					System.out.println("Doza medie: ");
					String avgDosage = reader.readLine();
					System.out.println("Data emiterii: ");
					Date releaseDate = Date.valueOf(reader.readLine());
					System.out.println("Id Angajat: ");
					int idEmployee = Integer.parseInt(reader.readLine());
					System.out.println("Id Medicament: ");
					int idMedicine = Integer.parseInt(reader.readLine());
					Prescription prescription = new Prescription(idPrescription, avgDosage, releaseDate, idEmployee, idMedicine);
					prescriptionOperation.addPrescription(prescription);
					break;
				case 2:
					System.out.println("Id: ");
					int idPrescriptionU = Integer.parseInt(reader.readLine());
					System.out.println("Doza medie: ");
					String avgDosageU = reader.readLine();
					System.out.println("Data emiterii: ");
					Date releaseDateU = Date.valueOf(reader.readLine());
					System.out.println("Id Angajat: ");
					int idEmployeeU = Integer.parseInt(reader.readLine());
					System.out.println("Id Medicament: ");
					int idMedicineU = Integer.parseInt(reader.readLine());
					prescriptionOperation.updatePrescription(idPrescriptionU, avgDosageU, releaseDateU, idEmployeeU, idMedicineU);
					break;

				case 3:
					System.out.println("Id: ");
					int idPrescriptionD = Integer.parseInt(reader.readLine());
					prescriptionOperation.deletePrescription(idPrescriptionD);
					break;
				case 4:
					List<Prescription> prescriptions = new ArrayList<Prescription>();
					prescriptions = prescriptionOperation.getAllPrescriptions();
					prescriptionOperation.printListOfPrescriptions(prescriptions);
					break;
				}
				break;
			case 6:
				System.out.println("Alegeti o optiune: ");
				System.out.println("1.Clientii care au in prescriptie medicamentul 1.");
				System.out.println("2.Clientii cu medicamentul 1 in prescriptii ordonati crescator.");
				System.out.println("3.Lista cu medicamentele aduse de Dealer-ul 1.");
				System.out.println("4.Prescriptiile date de angajatul 1.");
				System.out.println("5.Prescriptiile cu medicamentul 1");
				System.out.println("6.Prescriptiile cu medicamentul 1 date de angajatul 1.");
				System.out.println("7.Clientii care au primit prescriptii de la angajatul 1.");
				int optJoins = Integer.parseInt(reader.readLine());
				switch (optJoins) {

				case 0:
					break;
				case 1:
					List<Client> clients = clientOperation.join1();
					clientOperation.printListOfClients(clients);
					break;
				case 2:
					List<Client> clients2 = clientOperation.join2();
					clientOperation.printListOfClients(clients2);
					break;
				case 3:
					List<Medicine> medicines = medicineOperation.join3();
					medicineOperation.printListOfMedicines(medicines);
					break;
				case 4:
					List<Prescription> prescriptions = prescriptionOperation.join4();
					prescriptionOperation.printListOfPrescriptions(prescriptions);
					break;
				case 5:
					List<Prescription> prescriptions2 = prescriptionOperation.join5();
					prescriptionOperation.printListOfPrescriptions(prescriptions2);
					break;
				case 6:
					List<Prescription> prescriptions3 = prescriptionOperation.join6();
					prescriptionOperation.printListOfPrescriptions(prescriptions3);
					break;
				case 7:
					List<Client> clients3 = clientOperation.join7();
					clientOperation.printListOfClients(clients3);
					break;
				}
				break;
			default:
				System.out.println("This option does not exist.");
				System.exit(0);

			}

		}
	}
}
