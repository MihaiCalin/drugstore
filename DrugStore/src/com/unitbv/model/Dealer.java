package com.unitbv.model;

public class Dealer {

	private int idDealer;
	
	private String dealerName;
	
	private String address;
	
	private String phoneNumber;

	public Dealer(int idDealer, String dealerName, String address, String phoneNumber) {
		super();
		this.idDealer = idDealer;
		this.dealerName = dealerName;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}
	
	public Dealer() {
		
	}

	public int getIdDealer() {
		return idDealer;
	}

	public void setIdDealer(int idDealer) {
		this.idDealer = idDealer;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAdress(String adress) {
		this.address = adress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Dealer [idDealer=" + idDealer + ", dealerName=" + dealerName + ", address=" + address + ", phoneNumber="
				+ phoneNumber + "]";
	}
	
	
	
}
