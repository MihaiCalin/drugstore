package com.unitbv.model;

import java.sql.Date;

public class Prescription {

	private int idPrescription;
	
	private String avgDosage;
	
	private Date releaseDate;
	
	private int idEmployee;
	
	private int idMedicine;
	
	public Prescription() {
		
	}

	public Prescription(int idPrescription, String avgDosage, Date releaseDate, int idEmployee, int idMedicine) {
		super();
		this.idPrescription = idPrescription;
		this.avgDosage = avgDosage;
		this.releaseDate = releaseDate;
		this.idEmployee = idEmployee;
		this.idMedicine = idMedicine;
	}

	public int getIdPrescription() {
		return idPrescription;
	}

	public void setIdPrescription(int idPrescription) {
		this.idPrescription = idPrescription;
	}

	public String getAvgDosage() {
		return avgDosage;
	}

	public void setAvgDosage(String avgDosage) {
		this.avgDosage = avgDosage;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public int getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}

	public int getIdMedicine() {
		return idMedicine;
	}

	public void setIdMedicine(int idMedicine) {
		this.idMedicine = idMedicine;
	}

	@Override
	public String toString() {
		return "Prescription [idPrescription=" + idPrescription + ", avgDosage=" + avgDosage + ", releaseDate="
				+ releaseDate + ", idEmployee=" + idEmployee + ", idMedicine=" + idMedicine + "]";
	}
	
	
	
}
