package com.unitbv.model;

public class Medicine {

	private int idMedicine;
	
	private String medicineName;
	
	private int price;
	
	private int idDealer;

	public Medicine() {
		
	}

	public Medicine(int idMedicine, String medicineName, int price, int idDealer) {
		super();
		this.idMedicine = idMedicine;
		this.medicineName = medicineName;
		this.price = price;
		this.idDealer = idDealer;
	}

	public int getIdMedicine() {
		return idMedicine;
	}

	public void setIdMedicine(int idMedicine) {
		this.idMedicine = idMedicine;
	}

	public String getMedicineName() {
		return medicineName;
	}

	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getIdDealer() {
		return idDealer;
	}

	public void setIdDealer(int idDealer) {
		this.idDealer = idDealer;
	}

	@Override
	public String toString() {
		return "Medicine [idMedicine=" + idMedicine + ", medicineName=" + medicineName + ", price=" + price
				+ ", idDealer=" + idDealer + "]";
	}
	
	
}
