package com.unitbv.model;

public class Client {

	private int idClient;
	
	private String firstName;
	
	private String lastName;
	
	private String phoneNumber;
	
	private int idPrescription;
	
	public Client() {
		
	}

	public Client(int idClient, String firstName, String lastName, String phoneNumber, int idPrescription) {
		super();
		this.idClient = idClient;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.idPrescription = idPrescription;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getIdPrescription() {
		return idPrescription;
	}

	public void setIdPrescription(int idPrescription) {
		this.idPrescription = idPrescription;
	}

	@Override
	public String toString() {
		return "Client [idClient=" + idClient + ", firstName=" + firstName + ", lastName=" + lastName + ", phoneNumber="
				+ phoneNumber + ", idPrescription=" + idPrescription + "]";
	}
	
	
	
}
