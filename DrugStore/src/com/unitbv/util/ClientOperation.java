package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Client;

public class ClientOperation {

	private DatabaseConnection databaseConnection;

	public ClientOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	
	public boolean addClient(Client client) {
		databaseConnection.createConnection();
		String query = "INSERT INTO client VALUES (?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, client.getIdClient());
			ps.setString(2, client.getFirstName());
			ps.setString(3, client.getLastName());
			ps.setString(4,client.getPhoneNumber());
			ps.setInt(5, client.getIdPrescription());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	public List<Client> getAllClients() {
		databaseConnection.createConnection();
		String query = "SELECT idClient, firstName, lastName, phoneNumber, idPrescription FROM client";
		List<Client> clients = new ArrayList<Client>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Client client = new Client();
				client.setIdClient(rs.getInt("idClient"));
				client.setFirstName(rs.getString("firstName"));
				client.setLastName(rs.getString("lastName"));
				client.setPhoneNumber(rs.getString("phoneNumber"));
				client.setIdPrescription(rs.getInt("idPrescription"));
				clients.add(client);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return clients;
	}

	public void printListOfClients(List<Client> clients) {
		for (Client stud : clients) {
			System.out.println(stud.toString());
		}
	}

	public boolean updateClient(int idClient, String firstName, String lastName, String phoneNumber, int idPrescription) {
		databaseConnection.createConnection();

		String query = "UPDATE client SET firstName = ?, lastName = ?, phoneNumber = ?, idPrescription = ? WHERE idClient = ?";
		PreparedStatement ps = null;

		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, firstName);
			ps.setString(2, lastName);
			ps.setString(3, phoneNumber);
			ps.setInt(4, idPrescription);
			ps.setInt(5, idClient);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;

		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	public boolean deleteClient(int idClient) {
		databaseConnection.createConnection();

		String query = "DELETE FROM client WHERE idClient= ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, idClient);
			ps.executeUpdate();

		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/* Clientii care au in prescriptie medicamentul 1*/
	public List<Client> join1() {
		databaseConnection.createConnection();

		String query = "SELECT client.idClient, client.firstName, client.lastName, client.phoneNumber, client. idPrescription FROM client "
				+ "INNER JOIN prescription on client.idPrescription = prescription.idPrescription where prescription.idMedicine = 1;";

		List<Client> clients = new ArrayList<Client>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Client client = new Client();
				client.setIdClient(rs.getInt("idClient"));
				client.setFirstName(rs.getString("firstName"));
				client.setLastName(rs.getString("lastName"));
				client.setPhoneNumber(rs.getString("phoneNumber"));
				client.setIdPrescription(rs.getInt("idPrescription"));

				clients.add(client);

		}} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		 return clients;
	}
	/*Clientii cu medicamentul 1 in prescriptii ordonati crescator.*/
	public List<Client> join2() {
		databaseConnection.createConnection();

		String query = "SELECT client.idClient, client.firstName, client.lastName, client.phoneNumber, client. idPrescription "
				+ "FROM client INNER JOIN prescription on client.idPrescription = prescription.idPrescription where prescription.idMedicine = 1 "
				+ "ORDER BY client.firstName ASC;";

		List<Client> clients = new ArrayList<Client>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Client client = new Client();
				client.setIdClient(rs.getInt("idClient"));
				client.setFirstName(rs.getString("firstName"));
				client.setLastName(rs.getString("lastName"));
				client.setPhoneNumber(rs.getString("phoneNumber"));
				client.setIdPrescription(rs.getInt("idPrescription"));

				clients.add(client);

		}} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		 return clients;
	}
	
	/*Clientii care au primit prescriptii de la angajatul 1*/
	public List<Client> join7() {
		databaseConnection.createConnection();

		String query = "Select client.idClient, client.firstName from client "
				+ "inner join prescription on client.idPrescription = prescription.idPrescription "
				+ "inner join employee on prescription.idEmployee = employee.idEmployee "
				+ "where employee.idEmployee = 1;";

		List<Client> clients = new ArrayList<Client>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Client client = new Client();
				client.setIdClient(rs.getInt("idClient"));
				client.setFirstName(rs.getString("firstName"));

				clients.add(client);

		}} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		 return clients;
	}
}
