package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Client;
import com.unitbv.model.Medicine;

public class MedicineOperation {
	
	private DatabaseConnection databaseConnection;
	
	public MedicineOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	public boolean addMedicine(Medicine medicine) {
		databaseConnection.createConnection();
		String query = "INSERT INTO medicine VALUES (?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, medicine.getIdMedicine());
			ps.setString(2, medicine.getMedicineName());
			ps.setInt(3, medicine.getPrice());
			ps.setInt(4, medicine.getIdDealer());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	public List<Medicine> getAllMedicines() {
		databaseConnection.createConnection();
		String query = "SELECT idMedicine, medicineName, price, idDealer FROM medicine";
		List<Medicine> medicines = new ArrayList<Medicine>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Medicine medicine = new Medicine();
				medicine.setIdMedicine(rs.getInt("idMedicine"));
				medicine.setMedicineName(rs.getString("medicineName"));
				medicine.setPrice(rs.getInt("price"));
				medicine.setIdDealer(rs.getInt("idDealer"));
				medicines.add(medicine);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return medicines;
	}

	public void printListOfMedicines(List<Medicine> medicines) {
		for (Medicine stud : medicines) {
			System.out.println(stud.toString());
		}
	}

	public boolean updateMedicine(int idMedicine, String medicineName, int price, int idDealer) {
		databaseConnection.createConnection();

		String query = "UPDATE medicine SET medicineName = ?, price = ?, idDealer = ? WHERE idMedicine = ?";
		PreparedStatement ps = null;

		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, medicineName);
			ps.setInt(2, price);
			ps.setInt(3, idDealer);
			ps.setInt(4, idMedicine);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;

		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	public boolean deleteMedicine(int idMedicine) {
		databaseConnection.createConnection();

		String query = "DELETE FROM medicine WHERE idMedicine= ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, idMedicine);
			ps.executeUpdate();

		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	public List<Medicine> join3() {
		databaseConnection.createConnection();

		String query = "SELECT medicine.idMedicine, medicine.medicineName, medicine.price "
				+ "FROM medicine INNER JOIN dealer on medicine.idDealer = dealer.idDealer "
				+ "WHERE dealer.idDealer = 1;";

		List<Medicine> medicines = new ArrayList<Medicine>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Medicine medicine = new Medicine();
				medicine.setIdMedicine(rs.getInt("idMedicine"));
				medicine.setMedicineName(rs.getString("medicineName"));
				medicine.setPrice(rs.getInt("price"));
				//medicine.setIdDealer(rs.getInt("idDealer"));
				medicines.add(medicine);


		}} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		 return medicines;
	}
}
