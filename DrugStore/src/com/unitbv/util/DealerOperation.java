package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Dealer;

public class DealerOperation {
	private DatabaseConnection databaseConnection;
	
	public DealerOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	public boolean addDealer(Dealer dealer) {
		databaseConnection.createConnection();
		String query = "INSERT INTO dealer VALUES (?, ?, ?,?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, dealer.getIdDealer());
			ps.setString(2, dealer.getDealerName());
			ps.setString(3, dealer.getAddress());
			ps.setString(4,dealer.getPhoneNumber());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	public List<Dealer> getAllDealers() {
		databaseConnection.createConnection();
		String query = "SELECT idDealer, dealerName, address, phoneNumber FROM dealer";
		List<Dealer> dealers = new ArrayList<Dealer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Dealer dealer = new Dealer();
				dealer.setIdDealer(rs.getInt("idDealer"));
				dealer.setDealerName(rs.getString("dealerName"));
				dealer.setAdress(rs.getString("address"));
				dealer.setPhoneNumber(rs.getString("phoneNumber"));
				dealers.add(dealer);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return dealers;
	}

	public void printListOfDealers(List<Dealer> dealers) {
		for (Dealer stud : dealers) {
			System.out.println(stud.toString());
		}
	}

	public boolean updateDealer(int idDealer, String dealerName, String address, String phoneNumber) {
		databaseConnection.createConnection();

		String query = "UPDATE dealer SET dealerName=?, address = ?, phoneNumber = ? WHERE idDealer = ?";
		PreparedStatement ps = null;

		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, dealerName);
			ps.setString(2, address);
			ps.setString(3, phoneNumber);
			ps.setInt(4, idDealer);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;

		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	public boolean deleteDealer(int idDealer) {
		databaseConnection.createConnection();

		String query = "DELETE FROM dealer WHERE idDealer= ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, idDealer);
			ps.executeUpdate();

		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
}
