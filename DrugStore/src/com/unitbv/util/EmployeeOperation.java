package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Employee;

public class EmployeeOperation {
	
	private DatabaseConnection databaseConnection;

	public EmployeeOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	
	public boolean addEmployee(Employee Employee) {
		databaseConnection.createConnection();
		String query = "INSERT INTO employee VALUES (?, ?, ?,?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, Employee.getIdEmployee());
			ps.setString(2, Employee.getFirstName());
			ps.setString(3, Employee.getLastName());
			ps.setString(4,Employee.getPhoneNumber());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	public List<Employee> getAllEmployees() {
		databaseConnection.createConnection();
		String query = "SELECT idEmployee, firstName, lastName, phoneNumber FROM employee";
		List<Employee> employees = new ArrayList<Employee>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Employee employee = new Employee();
				employee.setIdEmployee(rs.getInt("idEmployee"));
				employee.setFirstName(rs.getString("firstName"));
				employee.setLastName(rs.getString("lastName"));
				employee.setPhoneNumber(rs.getString("phoneNumber"));
				employees.add(employee);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return employees;
	}

	public void printListOfEmployees(List<Employee> employees) {
		for (Employee stud : employees) {
			System.out.println(stud.toString());
		}
	}

	public boolean updateEmployee(int idEmployee, String firstName, String lastName, String phoneNumber) {
		databaseConnection.createConnection();

		String query = "UPDATE employee SET firstName = ?, lastName = ?, phoneNumber = ? WHERE idEmployee = ?";
		PreparedStatement ps = null;

		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, firstName);
			ps.setString(2, lastName);
			ps.setString(3, phoneNumber);
			ps.setInt(4, idEmployee);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;

		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	public boolean deleteEmployee(int idEmployee) {
		databaseConnection.createConnection();

		String query = "DELETE FROM employee WHERE idEmployee= ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, idEmployee);
			ps.executeUpdate();

		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	
}
