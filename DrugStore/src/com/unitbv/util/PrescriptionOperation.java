package com.unitbv.util;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Medicine;
import com.unitbv.model.Prescription;

public class PrescriptionOperation {

	private DatabaseConnection databaseConnection;

	public PrescriptionOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	
	public boolean addPrescription(Prescription prescription) {
		databaseConnection.createConnection();
		String query = "INSERT INTO prescription VALUES (?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, prescription.getIdPrescription());
			ps.setString(2, prescription.getAvgDosage());
			ps.setDate(3, (Date)prescription.getReleaseDate());
			ps.setInt(4, prescription.getIdEmployee());
			ps.setInt(5, prescription.getIdMedicine());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	public List<Prescription> getAllPrescriptions() {
		databaseConnection.createConnection();
		String query = "SELECT idPrescription, avgDosage, releaseDay, idEmployee, idPrescription FROM prescription";
		List<Prescription> prescriptions = new ArrayList<Prescription>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Prescription prescription = new Prescription();
				prescription.setIdPrescription(rs.getInt("idPrescription"));
				prescription.setAvgDosage(rs.getString("avgDosage"));
				prescription.setReleaseDate(rs.getDate("releaseDay"));
				prescription.setIdEmployee(rs.getInt("idEmployee"));
				prescription.setIdMedicine(rs.getInt("idPrescription"));
				prescriptions.add(prescription);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return prescriptions;
	}

	public void printListOfPrescriptions(List<Prescription> prescriptions) {
		for (Prescription stud : prescriptions) {
			System.out.println(stud.toString());
		}
	}

	public boolean updatePrescription(int idPrescription, String avgDosage, Date releaseDate, int idEmployee, int idMedicine) {
		databaseConnection.createConnection();

		String query = "UPDATE prescription SET avgDosage = ?, releaseDate = ?, idEmployee = ? WHERE idPrescription = ?";
		PreparedStatement ps = null;

		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, avgDosage);
			ps.setDate(2, releaseDate);
			ps.setInt(3, idEmployee);
			ps.setInt(4, idMedicine);
			ps.setInt(5, idPrescription);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;

		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	public boolean deletePrescription(int idPrescription) {
		databaseConnection.createConnection();

		String query = "DELETE FROM prescription WHERE idPrescription= ?";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, idPrescription);
			ps.executeUpdate();

		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	/* Prescriptiile date de angajatul 1. */
	
	public List<Prescription> join4() {
		databaseConnection.createConnection();

		String query = "SELECT prescription.idPrescription, prescription.avgDosage, prescription.releaseDay from prescription "
				+ "INNER JOIN employee on prescription.idEmployee = employee.idEmployee "
				+ "WHERE employee.idEmployee = 1;";

		List<Prescription> prescriptions = new ArrayList<Prescription>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Prescription prescription = new Prescription();
				prescription.setIdPrescription(rs.getInt("idPrescription"));
				prescription.setAvgDosage(rs.getString("avgDosage"));
				prescription.setReleaseDate(rs.getDate("releaseDay"));
				prescriptions.add(prescription);


		}} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		 return prescriptions;
	}
	
	/*Prescriptiile cu medicamentul 1*/
	
	public List<Prescription> join5() {
		databaseConnection.createConnection();

		String query = "SELECT prescription.idPrescription, prescription.avgDosage, prescription.releaseDay from prescription "
				+ "INNER JOIN medicine on prescription.idMedicine = medicine.idMedicine "
				+ "WHERE medicine.idMedicine = 1;";

		List<Prescription> prescriptions = new ArrayList<Prescription>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Prescription prescription = new Prescription();
				prescription.setIdPrescription(rs.getInt("idPrescription"));
				prescription.setAvgDosage(rs.getString("avgDosage"));
				prescription.setReleaseDate(rs.getDate("releaseDay"));
				prescriptions.add(prescription);


		}} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		 return prescriptions;
	}
	
/*Prescriptiile cu medicamentul 1 date de angajatul 1*/
	
	public List<Prescription> join6() {
		databaseConnection.createConnection();

		String query = "SELECT prescription.idPrescription, prescription.avgDosage, prescription.releaseDay from prescription "
				+ "INNER JOIN medicine on prescription.idMedicine = medicine.idMedicine "
				+ "INNER JOIN employee on prescription.idEmployee = employee.idEmployee "
				+ "WHERE medicine.idMedicine = 1 and employee.idEmployee = 1;";

		List<Prescription> prescriptions = new ArrayList<Prescription>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Prescription prescription = new Prescription();
				prescription.setIdPrescription(rs.getInt("idPrescription"));
				prescription.setAvgDosage(rs.getString("avgDosage"));
				prescription.setReleaseDate(rs.getDate("releaseDay"));
				prescriptions.add(prescription);


		}} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		 return prescriptions;
	}
}
